module sinf
  implicit none
  real(kind=8), allocatable, dimension(:) :: x,g

  contains

  subroutine sin_loop()
    implicit none
    integer :: i1
    real(kind=8), allocatable, dimension(:) :: s

    allocate(s(size(x)))

    do i1 = 1,size(x)
      s(i1) = sin(x(i1))
    end do
    print *, s(1)    

  end subroutine sin_loop

  subroutine sin_vec()
    implicit none
    integer :: i1
    real(kind=8), allocatable, dimension(:) :: s

    allocate(s(size(x)))

    s = sin(x)
    print *, s(1)

  end subroutine sin_vec


end module sinf

program test
  use sinf
  implicit none

    allocate(x(2000))
    call random_number(x)

    call sin_vec()

end program test
